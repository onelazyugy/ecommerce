package com.vietle.ecommerce.domain;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class AddItemToCartResponse {
    private Status status;
    private Item items;
}
