package com.vietle.ecommerce.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Status {
    private String status;
    private int statusCd;
    private String transId;
}
