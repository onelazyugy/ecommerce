package com.vietle.ecommerce.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Store {
    private int totalEmployee;
    private List<String> items;
    private String storeHour;
    private int totalItem;
}
