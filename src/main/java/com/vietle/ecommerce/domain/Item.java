package com.vietle.ecommerce.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Item {
    private int id;
    private String itemName;
    private String itemPrice;
    private String itemDescription;
}
