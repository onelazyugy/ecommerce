package com.vietle.ecommerce.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Catalog {
    private int totalItem;
    private List<String> items;
}
