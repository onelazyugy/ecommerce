package com.vietle.ecommerce.controller;

import com.vietle.ecommerce.domain.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//https://jasonwatmore.com/post/2019/06/22/angular-8-jwt-authentication-example-tutorial#auth-guard-ts
@RestController
@RequestMapping("/api/v1/")
public class AuthenticationController {
    public User authenticate() {
        User user = User.builder().username("fakeusername").token("jwt here").build();
        return user;
    }
}
