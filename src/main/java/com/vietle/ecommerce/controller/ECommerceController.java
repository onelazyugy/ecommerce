package com.vietle.ecommerce.controller;

import com.vietle.ecommerce.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class ECommerceController {
    @GetMapping("/store")
    public ResponseEntity<Store> fetchStoreDetials() {
        Store store = Store.builder().items(null).storeHour("8-5").totalEmployee(10).totalItem(100).build();
        ResponseEntity<Store> responseEntity = new ResponseEntity<>(store, HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping("/catalog/details")
    public ResponseEntity<Catalog> fetchCatalogDetails() {
        List<String> items = new ArrayList<>(3);
        items.add("shoe");
        items.add("shirt");
        items.add("pant");
        items.add("jacket");
        items.add("purse");
        Catalog catalog = Catalog.builder().totalItem(items.size()).items(items).build();
        ResponseEntity<Catalog> responseEntity = new ResponseEntity<>(catalog, HttpStatus.OK);
        return responseEntity;
    }

    @PostMapping("/add-item-to-cart")
    public ResponseEntity<AddItemToCartResponse> addItemToCart(@RequestBody Item item) {
        Status status = Status.builder().status("success").statusCd(200).transId(UUID.randomUUID().toString()).build();
        AddItemToCartResponse response = AddItemToCartResponse.builder().items(item).status(status).build();
        ResponseEntity<AddItemToCartResponse> responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
        return responseEntity;
    }
}
